package Pages;

import TestObjects.Email;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class EmailPage extends Page {

    @FindBy(xpath = "//div[@class='b-letter__head__subj__text']")
    @Title("Тема письма")
    private WebElement theme;

    @FindBy(xpath = "//div[@data-mnemo='from']/span//span")
    @Title("От кого")
    private WebElement from;

    @FindBy(xpath = "//div[@class='js-helper js-readmsg-msg']//div//div")
    @Title("Текст")
    private WebElement text;

    @FindBy(xpath = "//a[@id='PH_logoutLink']")
    @Title("Выход")
    private WebElement eixt;

    public void verifyEmail(Email obj) {
        Assert.assertEquals("Ошибка сравнения темы письма", obj.getTitle(), getElementValue(theme));
        Assert.assertEquals("Ошибка сравнения отправителя", obj.getFrom(), getElementValue(from));
        Assert.assertEquals("Ошибка сравнения содержания", obj.getText(), getElementValue(text));
        click(eixt);
    }
}
