package Pages;

import TestObjects.Email;

public class InboxPage extends Page {

    public void findEmailWithParams(Email obj) {
        final String emailXpath =
                String.format("//div[@class='b-datalist__item__addr' and contains(text(), '%s')]" +
                        "/preceding-sibling::div//div[@class='b-datalist__item__subj' and contains(text(), '%s')]" +
                        "/span[@class='b-datalist__item__subj__snippet' and contains(text(), '%s')]",
                obj.getFrom(), obj.getTitle(), obj.getText());

        click(emailXpath);
    }
}

