package Pages;

import TestObjects.Email;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Page {

    @FindBy(xpath = "//input[@id='mailbox:login']")
    @Title("Логин")
    private WebElement login;

    @FindBy(xpath = "//input[@id='mailbox:password']")
    @Title("Пароль")
    private WebElement password;

    @FindBy(xpath = "//input[@value='Войти']")
    @Title("Войти")
    private WebElement submit;

    public void fillForm(Email obj) {
        type(login, obj.getUser());
        type(password, obj.getPassword());
        click(submit);
    }
}
