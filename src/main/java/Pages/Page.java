package Pages;

import com.google.common.base.Function;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.time.Duration;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.TimeUnit;

public class Page {

    private static Logger LOG = LoggerFactory.getLogger(Page.class);

    Page() {
        PageFactory.initElements(Init.getWebDriver(), this);
        waitForPageBecomesReady();
    }

    private void waitForPageBecomesReady() {
        long timeOut = System.currentTimeMillis() + Init.getTimeOut();
        while (timeOut > System.currentTimeMillis()) {
            try {
                if ("complete".equalsIgnoreCase((String)((JavascriptExecutor)Init.getWebDriver())
                        .executeScript("return document.readyState", new Object[0]))) {
                    return;
                }
            } catch (Exception e) {
                LOG.error("troubles with JavascriptExecutor", e);
                Assert.fail(e.getMessage());
            }
        }
        throw new TimeoutException("Timeout of waiting till page becomes ready");
    }

    protected void type(WebElement element, String text) {
        if (null != text && !"".equals(text)) {
            element.click();
            try {
                element.clear();
            } catch (InvalidElementStateException e) {
                LOG.debug("Failed to clear element", e);
            }
            LOG.debug("typing element: " + getElementTitle(element));
            element.sendKeys(text);
        } else {
            LOG.debug("Do nothing, text is empty");
        }
    }

    protected void click(WebElement element) {
        LOG.debug("clicking element: " + getElementTitle(element));
        waitForElementPresent(element);
        element.click();
    }

    protected void click(String xpath) {
        waitForElementPresent(xpath);
        WebElement element = Init.getWebDriver().findElement(By.xpath(xpath));
        click(element);
    }

    protected String getElementValue(WebElement element) {
        String result = "";
        try {
            if (null == element.getText() || "".equals(element.getText())) {
                if (null == element.getAttribute("value") || "".equals(element.getAttribute("value"))) {
                   if (null != element.getAttribute("innerHTML") || !"".equals(element.getAttribute("innerHTML"))) {
                       return element.getAttribute("innerHTML");
                   }
                } else return element.getAttribute("value");
            } else return element.getText();
        } catch (NoSuchElementException|StaleElementReferenceException e) {
            Assert.fail("Element is not available");
        }
        return result;
    }

    private WebElement waitForElementPresent(final String xpath) {
       WebDriverWait wait = (WebDriverWait)new WebDriverWait(Init.getWebDriver(), Init.getTimeOut())
                .ignoring(StaleElementReferenceException.class)
                .ignoring(NoSuchElementException.class)
                .withMessage("No element, check xpath: " + xpath);
       return wait.until((Function<WebDriver, WebElement>) driver -> driver.findElement(By.xpath(xpath)));
    }

    private void waitForElementPresent(WebElement element) {
        WebDriverWait wait = (WebDriverWait)new WebDriverWait(Init.getWebDriver(), Init.getTimeOut())
                .ignoring(StaleElementReferenceException.class)
                .withMessage("Timeout wait for element becomes enable");
        wait.until((ExpectedCondition<Boolean>) webDriver -> element != null && element.isEnabled());
    }

    private String getElementTitle(WebElement element) {
        Class<?extends Page> clazz = this.getClass();
        String result = "No title for element";
        for(Field field : clazz.getDeclaredFields()) {
            if (field.getType() == WebElement.class && field.isAnnotationPresent(Title.class)) {
                field.setAccessible(true);
                try {
                    if(element == field.get(this)) {
                       Title title = field.getAnnotation(Title.class);
                       result =  (null != title) ? title.value() : "";
                       break;
                    }
                } catch (IllegalAccessException e) {
                    LOG.debug("Can't get annotation of element");
                }
            }
        }
        return result;
    }

}