package StepDefs;
import Pages.EmailPage;
import Pages.Init;
import Pages.LoginPage;
import Pages.InboxPage;
import TestObjects.Email;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.ru.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class CommonStepDefs {

    private static Logger LOG = LoggerFactory.getLogger(CommonStepDefs.class);

    @Before
    public void initData() {
        Init.getWebDriver();
    }

    @After
    public void freeData() {
        Init.getWebDriver().quit();
    }

    @Дано("^пользователь получает данные для проверки почты$")
    public void getEmailForTest() {
       final String dataPath = Init.getProperty("data.emails");
       List<Email> emails = Init.genEmailsFromJson(dataPath);
       Init.stash.put("emails", emails);
    }

    @SuppressWarnings("unchecked")
    private Email getObjectBySuite(String suite) {
        return ((List<Email>)  Init.stash.get("emails")).stream()
                .filter(i->i.getSuite().equals(suite)).collect(Collectors.toList()).get(0);
    }

    @Когда(value = "^пользователь авторизуется в почте из набора \"([^\"]*)\"$")
    public void performEmailAuth(String suite) {
        Email obj = getObjectBySuite(suite);
        Init.getWebDriver().get(obj.getUrl());
        new LoginPage().fillForm(obj);
    }

    @И("^пользователь находит письмо из набора \"([^\"]*)\" и открывает его$")
    public void searchAndNavigateToEmail(String suite)  {
        Email obj = getObjectBySuite(suite);
        new InboxPage().findEmailWithParams(obj);
    }

    @Тогда("^пользователь сверяет данные письма с данными из набора \"([^\"]*)\" и выходит$")
    public void verifyEmailDataAndExit(String suite) {
        Email obj = getObjectBySuite(suite);
        new EmailPage().verifyEmail(obj);
    }
}
